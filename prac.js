const Realm = require('realm');


const details =  {
    username: "admin",
    password: "password",
    serverUrl: "https://espresso.us1.cloud.realm.io"
};

const User = {
    name: 'User',
    properties: {
        name: 'string',
        username: 'string',
        password: 'string',
        email: 'string',
        dob: 'date',
        userType: 'string'
    }
}

Realm.Sync.User.login(details.serverUrl, details.username, details.password)
.then((user) => {
      let config = user.createConfiguration();
      config.schema = [User];
      Realm.open(config).then((realm) => {
        let results = realm.objects('User').filtered('name == "Someone123"');
        let subscription = results.subscribe();
        results.addListener((collection, change) => {
            console.log(collection);
        });
      }).catch((error) => {
          console.log(error);
      })
}).catch((error) => {
    console.log(error);
})